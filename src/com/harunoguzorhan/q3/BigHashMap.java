package com.harunoguzorhan.q3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class BigHashMap<K, V> extends HashMap<K, HashMap> {

	private static final long serialVersionUID = -2698000463368424863L;

	// private HashMap<K, V> childHashMap = new HashMap<>();

	public void put(K key, K childKey, V value) {
		HashMap<K, V> childHashMap = (HashMap<K, V>) get(key);
		childHashMap.put(childKey, value);
	}

	public void putChildren(K key) {
		HashMap hm = new HashMap();
		put(key, hm);
	}

	public List<K> listChildrenKeys(K key) {
		HashMap<K, HashMap> hm = get(key);
		K[] keys = (K[]) hm.keySet().toArray();
		return Arrays.asList(keys);
	}

	public List<V> listChildrenValues(K key) {
		List<V> childValues = new ArrayList<V>();
		K[] keys = (K[]) get(key).keySet().toArray();
		for (K childKey : keys) {
			childValues.add((V) get(key).get(childKey));
		}
		return childValues;
	}

	public V getChildrenValue(K key1, K key2) {
		return (V) get(key1).get(key2);
	}

	public V removeValueAtChildren(K key1, K childKey) {
		return (V) get(key1).remove(childKey);
	}

}

package com.harunoguzorhan.q3;

import java.util.List;

public class TestBigHashMap {

	public static void main(String[] args) {
		BigHashMap<String, String> cvMap = new BigHashMap<String, String>();
		cvMap.putChildren("harun");
		cvMap.put("harun", "primary school", "osmangazi i�o");
		cvMap.put("harun", "high school", "�engelk�y lisesi");
		cvMap.put("harun", "university", "marmara �niversitesi");
		cvMap.put("harun", "technology1", "java");
		cvMap.put("harun", "technology2", "spring");
		cvMap.put("harun", "technology3", "hibernate");
		cvMap.put("harun", "technology4", "maven");

		cvMap.putChildren("o�uzorhan");
		cvMap.put("o�uzorhan", "language1", "tr");
		cvMap.put("o�uzorhan", "language2", "en");
		cvMap.put("o�uzorhan", "reference1", "ref1name");
		cvMap.put("o�uzorhan", "reference2", "ref2name");
		cvMap.put("o�uzorhan", "reference3", "ref3name");

		System.out.println(cvMap.getChildrenValue("harun", "university"));

		List<String> harunKeysList = cvMap.listChildrenKeys("harun");
		System.out.println("harun's keys:");
		printListToConsole(harunKeysList);

		cvMap.removeValueAtChildren("harun", "technology1");
		
		List<String> harunChildrenList = cvMap.listChildrenValues("harun");
		System.out.println("harun's values:");
		printListToConsole(harunChildrenList);
	}

	public static void printListToConsole(List list) {
		for (Object object : list) {
			System.out.println(object);
		}
	}

}
